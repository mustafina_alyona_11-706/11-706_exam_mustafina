using System.Net;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using task2.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace task2.Controllers
{
    public class RestaurantController:Controller
    
    {
        private readonly AppContext _context;
        
        public RestaurantController(AppContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult Add()
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
            
            return View();
        }
        
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Add(Restaurant model)
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
            
            if (ModelState.IsValid)
            {
                var restaurant = new Restaurant();
                restaurant.Name = model.Name;
                _context.Restaurants.Add(restaurant);
                _context.SaveChanges();
//                var currentUserEmailClaim = HttpContext.User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType);
//                var currentUser = _context.Users.First(u => u.Email == currentUserEmailClaim.Value);
//                
//                var card = new Card
//                {
//                    PassportSeries = model.PassportSeries,
//                    PassportNumber = model.PassportNumber,
//                    UserId = currentUser.Id,
//                    User = currentUser
//                };
//
//                _context.Cards.Add(card);
//                _context.SaveChanges();

                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Неверные  данные");
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Detail(string id)
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;

            var restaurant = _context.Restaurants.First(r => r.Id.ToString() == id);
            var meals = _context.Meals.Where(m => m.RestaurantId == restaurant.Id);
            restaurant.Meals = meals.ToList();
               
            return View(restaurant);
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult AddMeal(string id)
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
            
            return View();
        }
        
        
        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AddMeal(string id, Meal model)
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
            
            if (ModelState.IsValid)
            {
                var meal = new Meal();
                meal.Name = model.Name;
                meal.Price = model.Price;
                meal.RestaurantId = int.Parse(id);
                var rest = _context.Restaurants.First(r => r.Id == meal.RestaurantId);
                meal.Restaurant = rest;
                _context.Meals.Add(meal);
                _context.SaveChanges();
//                var currentUserEmailClaim = HttpContext.User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType);
//                var currentUser = _context.Users.First(u => u.Email == currentUserEmailClaim.Value);
//                
//                var card = new Card
//                {
//                    PassportSeries = model.PassportSeries,
//                    PassportNumber = model.PassportNumber,
//                    UserId = currentUser.Id,
//                    User = currentUser
//                };
//
//                _context.Cards.Add(card);
//                _context.SaveChanges();

                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "Неверные  данные");
            return View(model);
        }
        
    }
}