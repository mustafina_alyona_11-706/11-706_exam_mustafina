using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using task2.Models;

namespace task2.Controllers
{
    public class OrderController: Controller
    {
        private readonly AppContext _context;
        
        public OrderController(AppContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Authorize(Roles = "user")]
        public ActionResult MakeOrder()
        {
            var meals = _context.Meals.ToList();
            return View(meals);
        }

        [HttpPost]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> MakeOrder(int[] selectedObjects)
        {
            var order = new Order();
            double total = 0;
            var currentUserEmailClaim = HttpContext.User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType);
            var currentUser = _context.Users.First(u => u.Email == currentUserEmailClaim.Value);
            
            foreach (int id in selectedObjects)
            {
                var meal = _context.Meals.First(m => m.Id == id);
                order.Meals.Add(meal);
                total += meal.Price;
            }

            order.UserId = currentUser.Id;
            order.TotalPrice = total;

            
           
            _context.Orders.Add(order);
            return Ok(total);
        }
    }
}