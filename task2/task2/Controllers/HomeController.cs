﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using task2.Models;

namespace task2.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppContext _context;
        
        public HomeController(AppContext context)
        {
            _context = context;
        }
                
        [Authorize(Roles = "admin, user")]
        public IActionResult Index()
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            string role = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;
        
            var rests = _context.Restaurants.ToList();
            return View(rests);
        }
        
        [Authorize(Roles = "admin")]
        public IActionResult Privacy()
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            ViewData["Message"] = "Your application privacy page.";
         
            return View();
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
        
        [HttpGet]
        public IActionResult Register()
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            if (ModelState.IsValid)
            {
                User user = await _context.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                if (user == null)
                {
                    user = new User
                    {
                        Email = model.Email, Password = model.Password
                    };
                    Role userRole = await _context.Roles.FirstOrDefaultAsync(r => r.Name == "user");
                    if (userRole != null)
                    {
                        user.Role = userRole;
                    }
        
                    _context.Users.Add(user);
        
                    await _context.SaveChangesAsync();
        
                    await Authenticate(user);
        
                    return RedirectToAction("Index");
                } 
                else
                {
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }
            }
        
            return View(model);
        }
                
        [HttpGet]
        public IActionResult Login()
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            } 
                    
            return View();
        }
                
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (HttpContext.User.HasClaim(c=>c.Value=="user"))
                ViewData["NavigationForUser"] = true;
                    
            if (ModelState.IsValid)
            {
                User user = await _context.Users
                    .Include(u => u.Role)
                    .FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(user); // аутентификация
         
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }
                
        private async Task Authenticate(User user)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login", "Home");
        }
    }
}