using Microsoft.EntityFrameworkCore;
using task2.Models;

namespace task2
{
    public class AppContext: DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) :
            base(options)
        {         
        }
        
        public DbSet<User> Users { get; set; }
        
        public DbSet<Role> Roles { get; set; }
        
        public DbSet<Meal> Meals { get; set; }
        
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Order> Orders { get; set; }
        
    }
}