using System.Collections.Generic;

namespace task2.Models
{
    public class Order
    {
        public int Id { get; set; }
        
        public int? UserId { get; set; }
        public User User { get; set; }
        
        public List<Meal> Meals { get; set; }
        
        public double TotalPrice { get; set; }

        public Order()
        {
            Meals = new List<Meal>();
        }
    }
}