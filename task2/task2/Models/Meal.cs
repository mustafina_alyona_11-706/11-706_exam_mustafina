using System.ComponentModel.DataAnnotations;

namespace task2.Models
{
    public class Meal
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Price { get; set; }
        
        public int? RestaurantId { get; set; }
        public Restaurant Restaurant{ get; set; }
    }
}