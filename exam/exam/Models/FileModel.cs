namespace exam.Models
{
    public class FileModel
    {
        public string Id { get; set; }
        public string ShortDescription { get; set; }
        public string FullDesciption { get; set; }
        
        public  string Path { get; set; }
        
    }
}