﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using exam.Models;
using Microsoft.AspNetCore.Hosting;

namespace exam.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppContext _context;
        IHostingEnvironment _appEnvironment;


        public HomeController(AppContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }
        public IActionResult Index()
        {
            var files = _context.Files.ToList();
            return View(files);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Upload(FileModel upload)
        {
            var form = Request.Form;
            var file = new FileModel();
            if (!string.IsNullOrEmpty(form["short"]))
            {
                file.ShortDescription = form["short"];
            }

            if (!string.IsNullOrEmpty(form["full"]))
            {
                file.FullDesciption = form["full"];
            }

            var uploadFile = Request.Form.Files["upload"];
            
            _context.Files.Add(file);
            _context.SaveChanges();

            var fileExt = System.IO.Path.GetExtension(uploadFile.FileName).Substring(1);
            string path =  file.Id+"."+fileExt;
            
            
//            using (var fileStream = new FileStream(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Files")+path, FileMode.Create))
//            {
//                await uploadFile.CopyToAsync(fileStream);
//            }

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Files", path);
            _context.Files.Last().Path = filePath;
            _context.SaveChanges();
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await uploadFile.CopyToAsync(fileStream);
            }

            
            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult Detail(string id)
        {
            var file = _context.Files.First(f => f.Id == id);

            return View(file);
        }
        
        public ActionResult Downloads()
        {
            var dir = new System.IO.DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Files\\"));
            System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
            List<string> items = new List<string>();

            foreach (var file in fileNames)
            {
                items.Add(file.Name);
            }

            return View("Index");
        }
        
    }

    
}