using exam.Models;
using Microsoft.EntityFrameworkCore;

namespace exam
{
    public class AppContext: DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) :
            base(options)
        {         
        }
        
        public DbSet<FileModel> Files { get; set; }
    }
}